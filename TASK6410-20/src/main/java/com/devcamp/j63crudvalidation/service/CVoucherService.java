package com.devcamp.j63crudvalidation.service;

import java.util.ArrayList;

import com.devcamp.j63crudvalidation.model.CVoucher;
import com.devcamp.j63crudvalidation.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }
}
