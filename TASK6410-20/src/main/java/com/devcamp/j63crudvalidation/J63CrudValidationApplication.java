package com.devcamp.j63crudvalidation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J63CrudValidationApplication {

	public static void main(String[] args) {
		SpringApplication.run(J63CrudValidationApplication.class, args);
	}

}
