package com.devcamp.j63crudvalidation.repository;

import com.devcamp.j63crudvalidation.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
}
