-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 05, 2022 lúc 05:10 PM
-- Phiên bản máy phục vụ: 10.4.24-MariaDB
-- Phiên bản PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `pizza_db`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL,
  `dia_chi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ho_ten` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `so_dien_thoai` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `dia_chi`, `email`, `ho_ten`, `ngay_cap_nhat`, `ngay_tao`, `so_dien_thoai`) VALUES
(1, 'Bình Dương', 'binhvo@gmail.com', 'BinhVo', 1620950400, 1620950400, '12345678'),
(2, 'Hồ Chí Minh', 'loanngo@gmail.com', 'NgoLoan', 1620950400, 1620950400, '12345678');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) NOT NULL,
  `ghi_chu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_cap_nhat` datetime DEFAULT NULL,
  `ngay_tao` datetime DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `drinks`
--

INSERT INTO `drinks` (`id`, `don_gia`, `ghi_chu`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`) VALUES
(1, 100000, 'Nước Cocacola', 'coca', '2022-08-05 21:46:10', '2022-08-05 21:46:10', 'COCACOLA'),
(2, 100000, 'Nước Pepsi', 'pepsi', '2022-08-05 21:46:10', '2022-08-05 21:46:10', 'Pepsi'),
(3, 100000, 'Nước Trà tắc', 'TRA_TAC', '2022-08-05 21:48:28', '2022-08-05 21:48:28', 'Trà Tắc'),
(4, 100000, 'Nước Trà Sữa', 'TRA_SUA', '2022-08-05 21:48:28', '2022-08-05 21:48:28', 'Trà Sữa'),
(5, 100000, 'Nước giải khát', 'NUOC_SAM', NULL, NULL, 'Nước Sâm');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) NOT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  `order_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paid` bigint(20) DEFAULT NULL,
  `pizza_size` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pizza_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `voucher_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`id`, `customer_id`, `order_code`, `paid`, `pizza_size`, `pizza_type`, `price`, `product_id`, `voucher_code`) VALUES
(1, 1, 'AAA', 123, 'small', 'Hawaii', 100000, 123, '10'),
(2, 2, 'BBB', 123, 'medium', 'Hải sản', 200000, 124, '20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` bigint(20) NOT NULL,
  `ma_san_pham` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ten_san_pham` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `don_gia` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `ma_san_pham`, `ten_san_pham`, `ngay_cap_nhat`, `ngay_tao`, `don_gia`) VALUES
(1, 'AAA', 'Bia AAA', 1620950400, 1620950400, 20000),
(2, 'BBB', 'Bia BBB', 1620950400, 1620950400, 20000);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_4ioautl9bhwm9nmdgersk8mjx` (`ma_nuoc_uong`),
  ADD UNIQUE KEY `UK_nvfpgsg41yfn7kycuqr5b7i82` (`ten_nuoc_uong`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
