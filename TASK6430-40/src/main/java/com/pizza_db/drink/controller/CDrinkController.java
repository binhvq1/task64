package com.pizza_db.drink.controller;

import java.util.*;

import javax.validation.Valid;

import com.pizza_db.drink.model.CDrink;
import com.pizza_db.drink.repository.IDrinkRepository;
import com.pizza_db.drink.service.CDrinkService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CDrinkController {
    @Autowired
    IDrinkRepository pDrinkRepository;
    @Autowired
    CDrinkService pDrinkService;
    // Lấy danh sách drink CÓ dùng service.
    @GetMapping("/drinks")// Dùng phương thức GET
    public ResponseEntity<List<CDrink>> getAllDrinksByService() {
        try {
            return new ResponseEntity<>(pDrinkService.getDrinkList(),HttpStatus.OK);            
        } catch (Exception e) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);   
        }
    }
    // Lấy drink theo {id} CÓ dùng service
    @GetMapping("/drinks/{id}")// Dùng phương thức GET
	public ResponseEntity<CDrink> getDrinkByIdWithService(@PathVariable("id") long id) {
		CDrink drink = pDrinkService.getDrinkById(id);
		if (drink != null) {
			return new ResponseEntity<>(drink, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
    // Tạo MỚI drink CÓ dùng service sử dụng phương thức POST
    @PostMapping("/drinks")// Dùng phương thức POST
	public ResponseEntity<Object> createDrinkWithService(@Valid @RequestBody CDrink pDrink) {
		CDrink drink = pDrinkService.createDrink(pDrink);
		if(drink == null) {
			return ResponseEntity.unprocessableEntity().body(" Drink already exist");
		} else {
			try {
				return new ResponseEntity<>(pDrinkRepository.save(pDrink), HttpStatus.CREATED);
			} catch (Exception e) {
				System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
				//return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
				return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: "+e.getCause().getCause().getMessage());
			}
		}
	}
    // Sửa/update drink theo {id} CÓ dùng service, sử dụng phương thức PUT
    @PutMapping("/drinks/{id}")// Dùng phương thức PUT
	public ResponseEntity<Object> updateDrinkByIdWithService(@PathVariable("id") long id, @Valid @RequestBody CDrink pDrink) {
		CDrink drink = pDrinkService.updateDrinkById(id, pDrink);
		if (drink != null) {
			try {
				return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified Drink:"+e.getCause().getCause().getMessage());
			}
		} else {
			return ResponseEntity.badRequest().body("Failed to get specified Drink: " + id + "  for update.");
		}

	}
    // Xoá/delete drink theo {id} KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/drinks/{id}")// Dùng phương thức DELETE
	public ResponseEntity<CDrink> deleteDrinkById(@PathVariable("id") long id) {
		try {
			pDrinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
    // Xoá/delete tất cả drink KHÔNG dùng service, sử dụng phương thức DELETE
    @DeleteMapping("/drinks")// Dùng phương thức DELETE
	public ResponseEntity<CDrink> deleteAllDrinks() {
		try {
			pDrinkRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
