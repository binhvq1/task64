package com.pizza_db.drink.repository;

import com.pizza_db.drink.model.CDrink;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
}
